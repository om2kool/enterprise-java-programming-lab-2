package com.jcasey;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hsqldb.jdbc.JDBCDataSource;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;


public class BookQuery extends ActionSupport {
	private static final String BOOK_ID = "BOOK_ID";
	private static final String TITLE = "TITLE";
	private static final String AUTHOR = "AUTHOR";
	private static final String GENRE = "GENRE";
	private static final  String ISBN = "ISBN";
	private static final  String BLURB = "BLURB";
	
	private String genre;
	private String title;
	private String author;

	ArrayList <Book> books = new ArrayList<Book>();

	class Book
	{		
		public Book() {
			genreList = new ArrayList<String>();
			genreList.add("Romance");
			genreList.add("Classic");
			genreList.add("Science Fiction");
			genreList.add("General");
			genreList.add("Dystopian Novella");
		}
		
		private int bookId;
		private String title;
		private String author;
		private String genre;
		private String isbn;
		private String blurb;
		private ArrayList<String> genreList;
		private String genreListSelect;
		
		public int getBookId() {
			return bookId;
		}
		public void setBookId(int bookId) {
			this.bookId = bookId;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getGenre() {
			return genre;
		}
		public void setGenre(String genre) {
			this.genre = genre;
		}
		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public ArrayList<String> getGenreList() {
			return genreList;
		}
		public void setGenreList(ArrayList<String> genreList) {
			this.genreList = genreList;
		}
		public String getGenreListSelect() {
			return genreListSelect;
		}
		public void setGenreListSelect(String genreListSelect) {
			this.genreListSelect = genreListSelect;
		}
		public String getBlurb() {
			return blurb;
		}
		public void setBlurb(String blurb) {
			this.blurb = blurb;
		}
	}
	
	@Override
	public void validate() {
		super.validate();
		if (StringUtils.isEmpty(getTitle())) {
			if (StringUtils.isEmpty(getAuthor())) {
				if (StringUtils.isEmpty(getGenre())) {
					addActionError("Error!!");
					addFieldError("title", "Title is blank");
					addFieldError("author", "Author is blank");
					addFieldError("genre", "Genre is blank");					
				}
			}			
		}
	}

	public String query()
	{		
//		try {
//			PrintWriter output = response.getWriter();
////			response.setContentType("text/html");
//			output.println("toasty");
//			output.flush();
//		
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			System.out.println(genre);
			System.out.println(title);
			System.out.println(author);
			
//			MysqlDataSource ds = new MysqlDataSource();
//
//			ds.setUser("root");
//			ds.setPassword("test");
//			ds.setServerName("localhost");
//			ds.setPort(3306);
//			ds.setDatabaseName("test");
			
			
			JDBCDataSource ds = new JDBCDataSource();
			
			// setup URL according to Oracle's specs
			ds.setUrl("jdbc:hsqldb:hsql://localhost/");
			
			// set other data source properties
			ds.setPassword("");
			ds.setUser("SA");
			
			conn = ds.getConnection();
			stmt = conn.createStatement();
			
			String querySQL = "select * from book"; 
			
			if(StringUtils.isNotBlank(title) || StringUtils.isNotBlank(author) || StringUtils.isNotBlank(genre))
			{
				querySQL = querySQL + " where";
			}
			
			if(StringUtils.isNotBlank(title))
			{
				querySQL = querySQL + " title ='"+title+"'";
			}
			
			if(StringUtils.isNotBlank(author))
			{
				querySQL = querySQL + " author ='"+author+"'";
			}
			
			if(StringUtils.isNotBlank(genre))
			{
				querySQL = querySQL + " genre ='"+genre+"'";
			}
			
			System.err.println(querySQL);
			
			rs = stmt.executeQuery(querySQL);
			
			// build up the output from the result set
			// keep iterating through the result set while another row exists
			
			while(rs.next() == true)
			{
				Book book = new Book();
				
				book.setBookId(rs.getInt(BOOK_ID));
				book.setTitle(rs.getString(TITLE));
				book.setAuthor(rs.getString(AUTHOR));
				book.setGenre(rs.getString(GENRE));
				book.setIsbn(rs.getString(ISBN));
				book.setBlurb(rs.getString(BLURB));
				
				books.add(book);
			}
			
			return Action.SUCCESS;
		}
		catch (SQLException e)
		{
			return Action.ERROR;
		}
		finally
		{

			if(rs != null)
			{
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(stmt != null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null)
			{
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}


	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}
}
